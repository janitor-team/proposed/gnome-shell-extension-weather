# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Jens Lody
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather 1.1\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: 2017-09-08 20:50+0300\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos+l10n@iki.fi>\n"
"Language-Team: \n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7.1\n"

#: src/extension.js:181
msgid "..."
msgstr "..."

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""

#: src/extension.js:519 src/extension.js:531
#, javascript-format
msgid "Can not connect to %s"
msgstr "Yhteys kohteeseen %s ei onnistu"

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr "Sijainnit"

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr "Lataa säätiedot uudelleen"

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr "Säätiedot tarjoaa:"

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr "Ei voida avata %s"

#: src/extension.js:887
msgid "Weather Settings"
msgstr "Sääasetukset"

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr "Virheellinen kaupunki"

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr "Virheellinen sijainti! Luo sijainti uudelleen."

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr "°F"

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr "K"

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr "°De"

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr "°N"

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr "°C"

#: src/extension.js:1055
msgid "Calm"
msgstr "Tyyntä"

#: src/extension.js:1058
msgid "Light air"
msgstr "Hiljainen tuuli"

#: src/extension.js:1061
msgid "Light breeze"
msgstr "Heikko tuuli"

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr "Heikonlainen tuuli"

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr "Kohtalainen tuuli"

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr "Navakka tuuli"

#: src/extension.js:1073
msgid "Strong breeze"
msgstr "Kova tuuli"

#: src/extension.js:1076
msgid "Moderate gale"
msgstr "Luja tuuli"

#: src/extension.js:1079
msgid "Fresh gale"
msgstr "Myrskyinen tuuli"

#: src/extension.js:1082
msgid "Strong gale"
msgstr "Myrsky"

#: src/extension.js:1085
msgid "Storm"
msgstr "Kova myrsky"

#: src/extension.js:1088
msgid "Violent storm"
msgstr "Ankara myrsky"

#: src/extension.js:1091
msgid "Hurricane"
msgstr "Hirmumyrsky"

#: src/extension.js:1095
msgid "Sunday"
msgstr "Sunnuntai"

#: src/extension.js:1095
msgid "Monday"
msgstr "Maanantai"

#: src/extension.js:1095
msgid "Tuesday"
msgstr "Tiistai"

#: src/extension.js:1095
msgid "Wednesday"
msgstr "Keskiviikko"

#: src/extension.js:1095
msgid "Thursday"
msgstr "Torstai"

#: src/extension.js:1095
msgid "Friday"
msgstr "Perjantai"

#: src/extension.js:1095
msgid "Saturday"
msgstr "Lauantai"

#: src/extension.js:1101
msgid "N"
msgstr "N"

#: src/extension.js:1101
msgid "NE"
msgstr "NE"

#: src/extension.js:1101
msgid "E"
msgstr "E"

#: src/extension.js:1101
msgid "SE"
msgstr "SE"

#: src/extension.js:1101
msgid "S"
msgstr "S"

#: src/extension.js:1101
msgid "SW"
msgstr "SW"

#: src/extension.js:1101
msgid "W"
msgstr "W"

#: src/extension.js:1101
msgid "NW"
msgstr "NW"

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr "bar"

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr "atm"

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr "at"

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr "torria"

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr "psi"

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr "mph"

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr "kn"

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1389
msgid "Loading ..."
msgstr "Ladataan..."

#: src/extension.js:1393
msgid "Please wait"
msgstr "Odota hetki"

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr "Pilvisyys:"

#: src/extension.js:1458
msgid "Humidity:"
msgstr "Kosteus:"

#: src/extension.js:1462
msgid "Pressure:"
msgstr "Paine:"

#: src/extension.js:1466
msgid "Wind:"
msgstr "Tuuli:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr "Eilen"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d päivä sitten"
msgstr[1] "%d päivää sitten"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ", "

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr "Tänään"

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr "Huomenna"

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "%d päivän päästä"
msgstr[1] "%d päivän päästä"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Ukkosmyrsky ja kevyttä sadetta"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "Ukkosmyrsky ja sadetta"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Ukkosmyrsky ja rankkasadetta"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "Kevyt ukkosmyrsky"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "Ukkosmyrsky"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "Voimakas ukkosmyrsky"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "Satunnaisia ukkosmyrskyjä"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Ukkosmyrsky ja kevyttä tihkusadetta"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Ukkosmyrsky ja tihkusadetta"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Ukkosmyrsky ja voimakasta tihkusadetta"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Kevyttä tihkusadetta"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "Tihkusadetta"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Voimakasta tihkusadetta"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Kevyttä tihkusadetta"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "Tihkusadetta"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Voimakasta tihkusadetta"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "Sadekuuroja ja tihkusadetta"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Voimakkaita sadekuuroja ja tihkusadetta"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "Tihkusadekuuroja"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "Kevyttä sadetta"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Kohtalaista sadetta"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Voimakasta sadetta"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Erittäin voimakasta sadetta"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "Äärimmäisen voimakasta sadetta"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "Jäätävää sadetta"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Kevyitä sadekuuroja"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Sadekuuroja"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Voimakkaita sadekuuroja"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Satunnaisia sadekuuroja"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "Kevyttä lumisadetta"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Lumisadetta"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "Voimakasta lumisadetta"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "Räntäsadetta"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "Räntäsadekuuroja"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "Kevyttä sadetta ja lunta"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "Sadetta ja lunta"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "Kevyitä lumikuuroja"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "Lumikuuroja"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "Voimakkaita lumikuuroja"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Usvaa"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Savua"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Usvaa"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Hiekka- ja pölypyörteitä"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "Sumua"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Hiekkaa"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Pölyistä"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "TULIVUORIPÖLYÄ"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "UKKOSPUUSKIA"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "TORNADO"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Taivas on selkeä"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Muutamia pilviä"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "Ajoittain pilvistä"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Melkein pilvistä"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "Pilvistä"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Ei saatavilla"

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
#, fuzzy
msgid "Searching ..."
msgstr "Ladataan..."

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Virheelliset tiedot etsiessä \"%s\""

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, javascript-format
msgid "\"%s\" not found"
msgstr "Ei löydetty \"%s\""

#: src/prefs.js:232
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"

#: src/prefs.js:233
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
msgid "Location"
msgstr "Sijainti"

#: src/prefs.js:350
msgid "Provider"
msgstr "Tarjoaja"

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr "Poistetaanko %s ?"

#: src/prefs.js:563
#, fuzzy
msgid "No"
msgstr "N"

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr "oletus"

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr "Muokkaa nimeä"

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
msgid "Clear entry"
msgstr "Tyhjennä tietue"

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr "Muokkaa koordinaatteja"

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr "Laajennusten säätietojen oletustarjoaja"

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr "Peru"

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr "Tallenna"

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr "Etsi sijaintia nimellä tai koordinaateilla"

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "esim. Vaiaku, Tuvalu tai -8.5211767,179.1976747"

#: data/weather-settings.ui:184
msgid "Find"
msgstr "Etsi"

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr "Säätietojen oletustarjoaja"

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr "Henkilökohtainen API-avain openweathermap.orgista"

#: data/weather-settings.ui:372
msgid "Personal Api key from Dark Sky"
msgstr "Henkilökohtainen API-avain Dark Skysta"

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr "Päivityksen aikakatkaisu nykyiselle säätiedoille [min]"

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr "Päivityksen aikakatkaisu sääennusteelle [min]"

#: data/weather-settings.ui:418
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""

#: data/weather-settings.ui:441
msgid "Use extensions api-key for openweathermap.org"
msgstr ""

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""

#: data/weather-settings.ui:462
msgid "Weather provider"
msgstr "Säätietojen tarjoaja"

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr "Valitse sijainnin tarjoaja"

#: data/weather-settings.ui:500
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"

#: data/weather-settings.ui:522
msgid "Geolocation provider"
msgstr "Sijainnin tarjoaja"

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Lämpötilan yksikkö"

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr "Tuulen nopeuden yksikkö"

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Paineen yksikkö"

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr "bofori"

#: data/weather-settings.ui:622
msgid "Units"
msgstr "Yksiköt"

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Sijainti paneelissa"

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Sijainti valikkolaatikolle [%] arvosta 0 (vasen) arvoon 100 (oikea)"

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Tuulen suunta nuolina"

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Käännä olosuhteet"

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Symboliset kuvakkeet"

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr "Teksti painikkeissa"

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Lämpötila paneelissa"

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Olosuhteet paneelissa"

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Olosuhteet ennusteessa"

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr "Keskitä ennuste"

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Päivien määrä ennusteessa"

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Desimaalipilkun jälkeen tulevien numeroiden maksimimäärä"

#: data/weather-settings.ui:747
msgid "Center"
msgstr "Keskellä"

#: data/weather-settings.ui:748
msgid "Right"
msgstr "Oikealla"

#: data/weather-settings.ui:749
msgid "Left"
msgstr "Vasemmalla"

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr "Asettelu"

#: data/weather-settings.ui:935
msgid "Version: "
msgstr "Versio: "

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr "tuntematon (koostettu itse?)"

#: data/weather-settings.ui:949
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Laajennus säätietojen näyttämiseksi <a href=\"https://openweathermap."
"org/\">Openweathermapista</a> tai <a href=\"https://darksky.net\">Dark "
"Skysta</a> melkein maailman jokaiseen kolkkaan.</span>"

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr "Ylläpitäjä"

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr "Verkkosivusto"

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Tämä ohjelma EI SISÄLLÄ MINKÄÄNLAISTA TAKUUTA.\n"
"Lue lisätietoja <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0."
"html\">GNU General Public -lisenssin versiosta 2 tai myöhemmästä</a>.</span>"

#: data/weather-settings.ui:997
msgid "About"
msgstr "Tietoja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Säätietojen tarjoaja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Sijainnin tarjoaja"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Tuulen nopeuden yksiköt"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Valitse yksikkö tuulen nopeudelle. Sallitut arvot ovat 'kph', 'mph', 'm/s', "
"'knots', 'ft/s' tai 'bofori'."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Valitse näytetäänkö tuulen suunta nuolina vai kirjaimina."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Näytettävä kaupunki"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Todellinen kaupunki"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Käytä tekstiä valikon painikkeissa"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Valikkolaatikon vaakasuuntainen sijainti."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Päivitysväli (säätiedot)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Päivitysväli (ennuste)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Keskitä ennustelaatikko."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Henkilökohtainen API-avaimesi openweathermap.orgista"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Henkilökohtainen API-avaimesi Dark Skysta"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Henkilökohtainen AppKey developer.mapquest.comista"
